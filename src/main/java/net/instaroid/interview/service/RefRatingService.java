/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.service;

import net.instaroid.interview.persistence.IOperations;
import net.instaroid.interview.persistence.model.RefRating;
import org.springframework.stereotype.Service;

/**
 *
 * @author MyAdmin
 */
@Service
public interface RefRatingService extends IOperations<RefRating, Integer> {
    
    public RefRating findByLikeMinGreatherThanAndLikeMaxLessThan(int likeMin);
    
}
