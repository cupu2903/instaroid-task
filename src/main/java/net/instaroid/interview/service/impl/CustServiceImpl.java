/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.service.impl;

import net.instaroid.interview.persistence.dao.CustomerDAO;
import net.instaroid.interview.persistence.model.Customer;
import net.instaroid.interview.service.AbstractService;
import net.instaroid.interview.service.CustService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MyAdmin
 */
@Service
@Transactional
public class CustServiceImpl extends AbstractService<Customer, Integer> implements CustService {

    @Autowired
    private CustomerDAO dao;

    @Override
    protected PagingAndSortingRepository<Customer, Integer> getDao() {
        return dao;
    }
}
