/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.service.impl;

import net.instaroid.interview.persistence.dao.RefRatingDAO;
import net.instaroid.interview.persistence.model.RefRating;
import net.instaroid.interview.service.AbstractService;
import net.instaroid.interview.service.RefRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MyAdmin
 */

@Service
@Transactional
public class RefRatingServiceImpl extends AbstractService<RefRating, Integer> implements RefRatingService {

    @Autowired
    RefRatingDAO dao;

    @Override
    public RefRating findByLikeMinGreatherThanAndLikeMaxLessThan(int likeMin) {
        return dao.selectByLike(likeMin);
    }

    @Override
    protected PagingAndSortingRepository<RefRating, Integer> getDao() {
        return dao;
    }

}
