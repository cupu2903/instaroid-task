package net.instaroid.interview.persistence.dao;

import net.instaroid.interview.persistence.model.RefRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RefRatingDAO extends JpaRepository<RefRating, Integer> {

    @Query(nativeQuery = true, value = "select * from ref_rating where like_min < :likeMin AND like_max > :likeMin")
    RefRating selectByLike(@Param("likeMin")int likeMin);

}
