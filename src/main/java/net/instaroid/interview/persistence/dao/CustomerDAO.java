package net.instaroid.interview.persistence.dao;

import net.instaroid.interview.persistence.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerDAO extends JpaRepository<Customer, Integer> {

}
