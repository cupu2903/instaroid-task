/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.persistence.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author MyAdmin
 */
@Entity
public class Customer implements Serializable {

    @Id
    @Column(name = "customer_id")
    private int customerId;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "no_of_likes")
    private int noOfLikes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rating", insertable = false, updatable = false)
    private RefRating refRating;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getNoOfLikes() {
        return noOfLikes;
    }

    public void setNoOfLikes(int noOfLikes) {
        this.noOfLikes = noOfLikes;
    }

    public RefRating getRefRating() {
        return refRating;
    }

    public void setRefRating(RefRating refRating) {
        this.refRating = refRating;
    }

}
