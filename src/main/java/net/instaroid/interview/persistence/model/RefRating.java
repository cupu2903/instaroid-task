/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.persistence.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author MyAdmin
 */
@Entity
@Table(name = "ref_rating")
public class RefRating implements Serializable {

    @Id
    @Column(name = "rating_id")
    private int ratingId;
    @Column(name = "like_min")
    private int likeMin;
    @Column(name = "like_max")
    private int likeMax;
    @Column(name = "stickers_available")
    private int noOfLikes;

    public int getRatingId() {
        return ratingId;
    }

    public void setRatingId(int ratingId) {
        this.ratingId = ratingId;
    }

    public int getLikeMin() {
        return likeMin;
    }

    public void setLikeMin(int likeMin) {
        this.likeMin = likeMin;
    }

    public int getLikeMax() {
        return likeMax;
    }

    public void setLikeMax(int likeMax) {
        this.likeMax = likeMax;
    }

    public int getNoOfLikes() {
        return noOfLikes;
    }

    public void setNoOfLikes(int noOfLikes) {
        this.noOfLikes = noOfLikes;
    }

    @Override
    public String toString() {
        return "RefRating{" + "ratingId=" + ratingId + ", likeMin=" + likeMin + ", likeMax=" + likeMax + ", noOfLikes=" + noOfLikes + '}';
    }

}
