/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.dto;

/**
 *
 * @author MyAdmin
 */
public class CustomerStickerDto {

    private int custId;
    private String custName;
    private int rating;
    private int stickerAvailable;

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getStickerAvailable() {
        return stickerAvailable;
    }

    public void setStickerAvailable(int stickerAvailable) {
        this.stickerAvailable = stickerAvailable;
    }

}
