/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.mapper;

import fr.xebia.extras.selma.Field;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;
import fr.xebia.extras.selma.Maps;
import net.instaroid.interview.dto.CustomerStickerDto;
import net.instaroid.interview.persistence.model.Customer;
import net.instaroid.interview.persistence.model.RefRating;

/**
 *
 * @author MyAdmin
 */
@Mapper(withIgnoreMissing = IgnoreMissing.ALL, withIoC = IoC.SPRING)
public interface ModelMapper {

    @Maps(withCustomFields = {
        @Field({"Customer.customerId", "CustomerStickerDto.custId"})
        ,@Field({"Customer.customerName", "CustomerStickerDto.custName"})
        ,@Field({"RefRating.ratingId", "CustomerStickerDto.rating"})
        , @Field({"RefRating.noOfStickers", "CustomerStickerDto.stickerAvailable"})})
    CustomerStickerDto asCustomerDto(Customer customer, RefRating rate);

}
