/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.instaroid.interview.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import net.instaroid.interview.dto.CustomerStickerDto;
import net.instaroid.interview.persistence.model.Customer;
import net.instaroid.interview.persistence.model.RefRating;
import net.instaroid.interview.service.CustService;
import net.instaroid.interview.service.RefRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MyAdmin
 */
@RestController
@RequestMapping(value = "/getStickers")
public class GetPhotoStiker {

    @Autowired
    CustService custService;

    @Autowired
    RefRatingService refRatingService;

    @GetMapping
    public List<CustomerStickerDto> getStickers(Principal principal) {
        List<CustomerStickerDto> customerStickerDto = new ArrayList<>();
        List<Customer> findAll = custService.findAll();
        for (Customer customer : findAll) {
            CustomerStickerDto csDto = new CustomerStickerDto();
            csDto.setCustId(customer.getCustomerId());
            csDto.setCustName(customer.getCustomerName());
            RefRating rate = refRatingService.findByLikeMinGreatherThanAndLikeMaxLessThan(customer.getNoOfLikes());
            csDto.setRating(rate.getRatingId());
            csDto.setStickerAvailable(rate.getNoOfLikes());
            customerStickerDto.add(csDto);
        }

        return customerStickerDto;
    }

}
